package com.example.democ.views;

public interface IsAcceptExchangeView {
    void isAcceptExchangeSuccess();
    void isAcceptExchangeFail();
}
