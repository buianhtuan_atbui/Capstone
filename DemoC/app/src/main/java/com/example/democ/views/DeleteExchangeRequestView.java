package com.example.democ.views;

public interface DeleteExchangeRequestView {
    void deleteExchangeRequestSuccess();
    void deleteExchangeRequestFail();

}
