package com.example.democ.iclick;

import android.content.Context;

import com.example.democ.model.GardenResult;

public interface IClickGarden {
    void clickGarden(GardenResult gardenResult);
}
