package com.example.democ.iclick;

import com.example.democ.model.PostData;
import com.example.democ.model.ShareData;

public interface IClickPost {
    void clickBtnExchange(PostData shareData);
    void clickPosterUser(PostData shareData);
}
