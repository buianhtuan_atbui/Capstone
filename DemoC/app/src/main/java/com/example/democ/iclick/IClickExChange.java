package com.example.democ.iclick;

import com.example.democ.model.ExchangeData;

public interface IClickExChange {
    void clickExchange(ExchangeData exchangeData, int positionClick);
}
